package template

import (
	"golang.org/x/text/language"
)

var matcher = language.NewMatcher([]language.Tag{
	language.SimplifiedChinese,
	language.TraditionalChinese,
	language.English,
}, language.PreferSameScript(true))
