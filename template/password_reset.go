package template

import (
	"io"
	"strings"

	"github.com/wzshiming/mail"
	_ "gitlab.com/genned/mail/catalog"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	hermes "gopkg.in/matcornic/hermes.v1"
)

func GeneratePasswordResetEmail(lang string, product, from, to, link string) (io.WriterTo, error) {
	name := to[:strings.Index(to, "@")]
	tag, _ := language.MatchStrings(matcher, lang)

	p := message.NewPrinter(tag)

	product = p.Sprintf("%s", product)
	copyright := p.Sprintf("Copyright © 2019 %s. All rights reserved.", product)
	trouble := p.Sprintf("If you’re having trouble with the button '%s', copy and paste the URL below into your web browser.", "{ACTION}")
	greeting := p.Sprintf("Hi")
	instruction := p.Sprintf("click this button to verify your Email:")
	button := p.Sprintf("Click verify and set password")
	outro := p.Sprintf("If this validation message is not what you requested, ignore it. It does not affect the security of your mailbox.")
	signature := p.Sprintf("Greetings from %s", product)
	subject := p.Sprintf("Please check the password setting link")

	h := &hermes.Hermes{
		Theme:         new(hermes.Default),
		TextDirection: hermes.TDRightToLeft,
		Product: hermes.Product{
			Name:        product,
			Copyright:   copyright,
			TroubleText: trouble,
		},
	}
	email := hermes.Email{
		Body: hermes.Body{
			Greeting: greeting,
			Name:     name,
			Actions: []hermes.Action{
				{
					Instructions: instruction,
					Button: hermes.Button{
						Color: "#f0514a",
						Text:  button,
						Link:  link,
					},
				},
			},
			Outros: []string{
				outro,
			},
			Signature: signature,
		},
	}
	emailBody, err := h.GenerateHTML(email)
	if err != nil {
		return nil, err
	}
	msg := mail.NewMessage()
	msg.SetAddressHeader("From", from, product)
	msg.SetHeader("To", to)
	msg.SetHeader("Subject", subject)
	msg.SetBody("text/html", emailBody)
	return msg, nil
}
