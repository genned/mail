// Code generated; Do not regenerate the overwrite after editing.

package mail

import (
	"github.com/wzshiming/mail"
	"gitlab.com/genned/mail/template"
)

type Config struct {
	Product  string
	From     string
	Host     string
	Port     int
	Username string
	Password string
}

// MailService is service of the Mail
// #path:"/mail/"#
type MailService struct {
	from    string
	product string
	dialer  *mail.Dialer
}

// NewMailService Create a new MailService
func NewMailService(config *Config) (*MailService, error) {
	return &MailService{
		from:    config.From,
		product: config.Product,
		dialer:  mail.NewDialer(config.Host, config.Port, config.Username, config.Password),
	}, nil
}

// PasswordReset a Mail
// #route:"POST /password_reset/{to}"#
func (s *MailService) PasswordReset(to string, link *PasswordReset, lang string) (err error) {
	msg, err := template.GeneratePasswordResetEmail(lang, s.product, s.from, to, link.Link)
	if err != nil {
		return err
	}

	sc, err := s.dialer.Dial()
	if err != nil {
		return err
	}
	defer sc.Close()

	return sc.Send(s.from, []string{to}, msg)
}
