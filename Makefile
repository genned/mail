generate:
	mkdir -p catalog
	cd catalog && gotext -srclang=en update -out=catalog.go -lang=en,zh-hans,zh-hant ../template

install_tools:
	go get golang.org/x/text/cmd/gotext